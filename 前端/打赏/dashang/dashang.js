let bd = document.querySelector('body');
let qbox = bd.querySelector('#QRBox');
let mbox = bd.querySelector('#MainBox');
let dbox = bd.querySelector('#PayBox');
//定义二维码图片地址
let qqr = './dashang/qr/QQPay.jpeg';
let aqr = './dashang/qr/AliPay.jpeg';
let wqr = './dashang/qr/WeChat.jpeg';

let showQR = (QR) => {
    if (QR) mbox.style.backgroundImage = `url(${QR})`;
    qbox.classList.add('fadeIn');
    mbox.classList.add('showQR');
};

dbox.addEventListener('click', (e) => {
    let el = e.target;
    if (el.id === 'QQPay') {
        showQR(qqr);
    } else if (el.id === 'AliPay') {
        showQR(aqr);
    } else if (el.id === 'WeChat') {
        showQR(wqr);
    }
});

qbox.addEventListener('click', () => {
    mbox.classList.remove('showQR');
    mbox.classList.add('hideQR');
    setTimeout(a => {
        qbox.classList.remove('fadeIn');
        mbox.classList.remove('hideQR');
    }, 600)
});
