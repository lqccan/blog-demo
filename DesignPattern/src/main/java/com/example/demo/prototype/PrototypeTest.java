package com.example.demo.prototype;

import cn.hutool.core.date.StopWatch;

public class PrototypeTest{

    public static void main(String[] args) {
        StopWatch stopWatch = new StopWatch();
        int num = 100;

        //new方式
        stopWatch.start();
        for (int i = 0; i < num; i++) {
            PrototypeObject object = new PrototypeObject();
        }
        stopWatch.stop();
        System.out.println("new方式耗时："+stopWatch.getLastTaskTimeMillis());

        //原型克隆模式
        stopWatch.start();
        //第一次生成一个对象用于克隆
        PrototypeObject object = new PrototypeObject();
        for (int i = 0; i < num-1; i++) {
            PrototypeObject clone = object.clone();
        }
        stopWatch.stop();
        System.out.println("原型克隆模式耗时："+stopWatch.getLastTaskTimeMillis());
    }

}
