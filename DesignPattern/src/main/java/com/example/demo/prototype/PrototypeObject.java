package com.example.demo.prototype;

/**
 * 原型测试对象，实现了克隆接口
 */
public class PrototypeObject implements Cloneable{

    public PrototypeObject() {
        try {
            //模拟创建对象耗时
            Thread.sleep(100);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public PrototypeObject clone() {
        try {
            return (PrototypeObject) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }

}
