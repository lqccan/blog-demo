package com.example.demo.builder;

public class BuilderTest {

    public static void main(String[] args) {
        SimpleDataSource dataSource = SimpleDataSource.builder()
                .setUrl("url")
                .setUsername("username")
                .setPassword("password")
                .setInitialSize(1)
                .setMaxSize(100000)
                .build();
        System.out.println(dataSource);
    }

}
