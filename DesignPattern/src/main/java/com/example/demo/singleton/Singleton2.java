package com.example.demo.singleton;

public class Singleton2 {

    private Singleton2() {}

    private static Singleton2 instance;

    static {
        //do something
        instance = new Singleton2();
    }

    public static Singleton2 getInstance(){
        return instance;
    }

}
