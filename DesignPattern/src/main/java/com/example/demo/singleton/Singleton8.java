package com.example.demo.singleton;

public class Singleton8 {

    private Singleton8() {}

    private enum SingletonGet{
        INSTANCE;
        private Singleton8 instance;

        SingletonGet() {
            instance = new Singleton8();
        }

        public Singleton8 getInstance() {
            return instance;
        }
    }

    public static Singleton8 getInstance(){
        return SingletonGet.INSTANCE.getInstance();
    }

}
