package com.example.demo.singleton;

public class Singleton7 {

    private Singleton7() {}

    private static class SingletonGet{
        private static final Singleton7 instance = new Singleton7();
    }

    public static Singleton7 getInstance(){
        return SingletonGet.instance;
    }

}
