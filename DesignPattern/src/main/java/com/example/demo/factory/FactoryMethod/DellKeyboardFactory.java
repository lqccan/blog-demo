package com.example.demo.factory.FactoryMethod;

import com.example.demo.factory.product.impl.DellKeyboard;
import com.example.demo.factory.product.Keyboard;

/**
 * 戴尔键盘工厂
 */
public class DellKeyboardFactory implements KeyboardFactory {
    @Override
    public Keyboard getKeyboard() {
        return new DellKeyboard();
    }
}
