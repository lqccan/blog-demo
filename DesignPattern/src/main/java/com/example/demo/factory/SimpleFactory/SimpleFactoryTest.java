package com.example.demo.factory.SimpleFactory;

import com.example.demo.factory.product.Keyboard;

public class SimpleFactoryTest {

    public static void main(String[] args) {
        SimpleFactory factory = new SimpleFactory();
        Keyboard keyboard;

        //联想
        keyboard = factory.getKeyboard("lenovo");
        keyboard.typing();

        //戴尔
        keyboard = factory.getKeyboard("dell");
        keyboard.typing();

        //其他牌子
        keyboard = factory.getKeyboard("other");
        keyboard.typing();
    }

}
