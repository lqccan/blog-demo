package com.example.demo.factory.FactoryMethod;

import com.example.demo.factory.product.Keyboard;

/**
 * 键盘工厂
 */
public interface KeyboardFactory {

    Keyboard getKeyboard();

}
