package com.example.demo.factory.FactoryMethod;

import com.example.demo.factory.product.Keyboard;

/**
 * 工厂方法测试
 */
public class FactoryMethodTest {

    public static void main(String[] args) {
        KeyboardFactory factory;
        Keyboard keyboard;

        //联想
        factory = new LenovoKeyboardFactory();
        keyboard = factory.getKeyboard();
        keyboard.typing();

        //戴尔
        factory = new DellKeyboardFactory();
        keyboard = factory.getKeyboard();
        keyboard.typing();
    }

}
