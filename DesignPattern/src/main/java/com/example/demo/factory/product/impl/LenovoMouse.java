package com.example.demo.factory.product.impl;

import com.example.demo.factory.product.Mouse;

public class LenovoMouse implements Mouse {
    @Override
    public void click() {
        System.out.println("LenovoMouse");
    }
}
