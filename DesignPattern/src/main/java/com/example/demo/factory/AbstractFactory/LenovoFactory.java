package com.example.demo.factory.AbstractFactory;

import com.example.demo.factory.product.Keyboard;
import com.example.demo.factory.product.Mouse;
import com.example.demo.factory.product.impl.LenovoKeyboard;
import com.example.demo.factory.product.impl.LenovoMouse;

/**
 * 联想工厂
 */
public class LenovoFactory implements AbstractFactory {
    @Override
    public Keyboard getKeyboard() {
        return new LenovoKeyboard();
    }

    @Override
    public Mouse getMouse() {
        return new LenovoMouse();
    }
}
