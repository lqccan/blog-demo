package com.example.demo.factory.AbstractFactory;

import com.example.demo.factory.product.Keyboard;
import com.example.demo.factory.product.Mouse;

public class AbstractFactoryTest {

    public static void main(String[] args) {
        AbstractFactory factory;
        Keyboard keyboard;
        Mouse mouse;

        //联想系列
        factory = new LenovoFactory();
        keyboard = factory.getKeyboard();
        mouse = factory.getMouse();
        keyboard.typing();
        mouse.click();

        //戴尔系列
        factory = new DellFactory();
        keyboard = factory.getKeyboard();
        mouse = factory.getMouse();
        keyboard.typing();
        mouse.click();
    }

}
