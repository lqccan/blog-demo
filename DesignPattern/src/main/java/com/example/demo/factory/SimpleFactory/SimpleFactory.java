package com.example.demo.factory.SimpleFactory;

import com.example.demo.factory.product.impl.DellKeyboard;
import com.example.demo.factory.product.Keyboard;
import com.example.demo.factory.product.impl.LenovoKeyboard;

/**
 * 简单工厂
 */
public class SimpleFactory {

    public Keyboard getKeyboard(String brand) {
        if ("lenovo".equals(brand)) {
            return new LenovoKeyboard();
        } else if ("dell".equals(brand)) {
            return new DellKeyboard();
        } else {
            throw new RuntimeException("不支持该品牌：" + brand);
        }
    }

}
