package com.example.demo.factory.AbstractFactory;

import com.example.demo.factory.product.Keyboard;
import com.example.demo.factory.product.Mouse;

/**
 * 抽象工厂
 */
public interface AbstractFactory {

    Keyboard getKeyboard();

    Mouse getMouse();

}
