package com.example.demo.factory.FactoryMethod;

import com.example.demo.factory.product.Keyboard;
import com.example.demo.factory.product.impl.LenovoKeyboard;

/**
 * 联想键盘工厂
 */
public class LenovoKeyboardFactory implements KeyboardFactory {
    @Override
    public Keyboard getKeyboard() {
        return new LenovoKeyboard();
    }
}
