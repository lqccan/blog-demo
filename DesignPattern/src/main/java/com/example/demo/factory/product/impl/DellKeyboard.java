package com.example.demo.factory.product.impl;

import com.example.demo.factory.product.Keyboard;

/**
 * 戴尔键盘
 */
public class DellKeyboard implements Keyboard {
    @Override
    public void typing() {
        System.out.println("DellKeyboard");
    }
}
