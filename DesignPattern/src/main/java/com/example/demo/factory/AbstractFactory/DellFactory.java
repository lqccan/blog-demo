package com.example.demo.factory.AbstractFactory;

import com.example.demo.factory.product.Keyboard;
import com.example.demo.factory.product.Mouse;
import com.example.demo.factory.product.impl.DellKeyboard;
import com.example.demo.factory.product.impl.DellMouse;

/**
 * 戴尔工厂
 */
public class DellFactory implements AbstractFactory {
    @Override
    public Keyboard getKeyboard() {
        return new DellKeyboard();
    }

    @Override
    public Mouse getMouse() {
        return new DellMouse();
    }
}
