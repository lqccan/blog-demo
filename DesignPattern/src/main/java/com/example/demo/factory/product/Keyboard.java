package com.example.demo.factory.product;

/**
 * 键盘接口
 */
public interface Keyboard {

    /**
     * 打字
     */
    void typing();

}
