package com.example.demo.factory.product.impl;

import com.example.demo.factory.product.Keyboard;

/**
 * 联想键盘
 */
public class LenovoKeyboard implements Keyboard {
    @Override
    public void typing() {
        System.out.println("LenovoKeyboard");
    }
}
