package com.example.demo.factory.product;

/**
 * 鼠标接口
 */
public interface Mouse {

    /**
     * 点击
     */
    void click();

}
