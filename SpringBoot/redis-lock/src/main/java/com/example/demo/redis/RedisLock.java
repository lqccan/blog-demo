package com.example.demo.redis;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * redis分布式锁注解
 * 1、存在多个注解嵌套的情况，仅最外层注解有效，内层注解将被忽略
 * 2、锁的key值就是在redis中的key值，key支持SpringEL表达式，可以实现从入参动态获取key值
 * 例：使用学生的名字和老师的名字作为rediskey
 *      注解：@RedisCoucurrentLock(params="#student.name+#teacher.name")
 *      方法：test(student,teacher)
 *      加前缀：@RedisCoucurrentLock(params="'前缀'+#student.name+#teacher.name")
 *      <b>注意<b/>：使用时需要保证指定规则生成的key值在业务上唯一，避免key值冲突
 *                  建议使用业务单据号或者主键作为key值，如需使用主键id作为key，一定要加业务前缀避免冲突。
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface RedisLock {

    /**
     * 锁的key
     * @return
     */
    String key();

    /**
     * 超时时间(秒，默认300秒)
     * @return
     */
    long timeout() default 300L;

    /**
     * 加锁失败提示语
     * @return
     */
    String msg() default "请求已发起，请勿重复操作！";

}
