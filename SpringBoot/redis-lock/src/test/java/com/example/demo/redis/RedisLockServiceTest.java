package com.example.demo.redis;

import com.example.demo.DemoApplicationTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class RedisLockServiceTest extends DemoApplicationTest {

    @Autowired
    private RedisLockService redisLockService;

    @Test
    public void runWithLock() {
        boolean b = redisLockService.runWithLock(() -> {
            System.out.println("test");
        }, "lock", "lock");
        System.out.println(b);
    }

    @Test
    public void lock() {
        boolean lock = redisLockService.lock("lock", "lock");
        System.out.println(lock);
    }

    @Test
    public void unlock() {
        boolean lock = redisLockService.unlock("lock", "lock");
        System.out.println(lock);
    }
}