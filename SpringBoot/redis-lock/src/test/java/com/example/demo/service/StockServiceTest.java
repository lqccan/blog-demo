package com.example.demo.service;

import com.example.demo.DemoApplicationTest;
import org.junit.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class StockServiceTest extends DemoApplicationTest {

    /**
     * 并发数
     */
    private static final int threads = 1000;
    /**
     * 扣库存次数，即库存数
     */
    private static final int nums = threads * 100;
    /**
     * 执行扣库存的线程池
     */
    private static final ExecutorService executor = Executors.newFixedThreadPool(threads);

    /**
     * 直接减库存，库存数量错误
     * @throws InterruptedException
     */
    @Test
    public void test1() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(nums);
        StockService stockService = applicationContext.getBean(StockService.class, nums);
        stockService.printStock();
        for (int i = 0; i < nums; i++) {
            executor.execute(()->{
                stockService.subStock();
                latch.countDown();
            });
        }
        latch.await();
        stockService.printStock();
    }

    @Test
    public void test2() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(nums);
        StockService stockService = applicationContext.getBean(StockService.class, nums);
        stockService.printStock();
        AtomicInteger subNums = new AtomicInteger(0);
        for (int i = 0; i < nums; i++) {
            executor.execute(()->{
                stockService.subStockWithLock();
                subNums.incrementAndGet();
                latch.countDown();
            });
        }
        latch.await();
        stockService.printStock();
    }

}