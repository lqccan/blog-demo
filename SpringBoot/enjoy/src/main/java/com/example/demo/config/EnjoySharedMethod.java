package com.example.demo.config;

/**
 * 提供给enjoy在视图上使用的方法
 */
public class EnjoySharedMethod {

    /**
     * 示例方法
     * html页面中使用：#(test())
     * @return
     */
    public String test(){
        return "EnjoySharedMethod.test()输出结果";
    }

}
