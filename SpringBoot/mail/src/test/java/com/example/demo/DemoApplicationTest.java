package com.example.demo;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.map.MapUtil;
import com.jfinal.template.Engine;
import com.jfinal.template.Template;
import com.jfinal.template.source.ClassPathSourceFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.test.context.junit4.SpringRunner;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Map;

@SpringBootTest
@RunWith(SpringRunner.class)
public class DemoApplicationTest {

    @Value("${spring.mail.username}")
    private String myMail;

    private String toMail = "toMail@qq.com";

    @Autowired
    private JavaMailSender jms;

    @Before
    public void befor(){
        ClassPathSourceFactory classPathSourceFactory = new ClassPathSourceFactory();
        Engine.create("mail")
                .setBaseTemplatePath("/templates/")
                .setDatePattern("yyyy-MM-dd HH:mm:ss")
                .setSourceFactory(classPathSourceFactory);
    }

    @Test
    public void contextLoads() {
        System.out.println("测试");
    }

    @Test
    public void textMail() {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(myMail);
        message.setTo(toMail);
        message.setSubject("textMail发送");
        message.setText("内容123456");
        jms.send(message);
    }

    @Test
    public void htmlMail() {
        MimeMessage mimeMessage = jms.createMimeMessage();
        try {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true);
            messageHelper.setFrom(myMail);
            messageHelper.setTo(toMail);
            messageHelper.setSubject("htmlMail发送");
            Template template = Engine.use("mail").getTemplate("notice.html");
            Map<String, Object> data = MapUtil.newHashMap();
            data.put("title", "标题");
            data.put("time", DateUtil.now());
            data.put("content", "内容");
            String html = template.renderToString(data);
            messageHelper.setText(html, true);
            jms.send(mimeMessage);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

}