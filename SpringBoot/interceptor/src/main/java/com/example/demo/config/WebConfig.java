package com.example.demo.config;

import com.example.demo.interceptor.AInterceptor;
import com.example.demo.interceptor.BInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * web配置
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Autowired
    private AInterceptor aInterceptor;

    @Autowired
    private BInterceptor bInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(aInterceptor)
                .addPathPatterns("/**");
        registry.addInterceptor(bInterceptor)
                .addPathPatterns("/**");

    }

}
