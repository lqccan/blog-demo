package com.example.demo.config;

import com.jfinal.template.ext.spring.JFinalViewResolver;
import com.jfinal.template.source.ClassPathSourceFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * enjoy视图模板的配置
 */
@Configuration
public class EnjoyConfig {

    @Autowired
    private ProfileConfig profileConfig;

    @Bean
    public JFinalViewResolver jFinalViewResolver() {
        JFinalViewResolver jfr = new JFinalViewResolver();
        //项目在开发环境下则开启视图的开发模式
        if (profileConfig.isDev()){
            jfr.setDevMode(true);
        }
        //视图中访问session中的内容
        jfr.setSessionInView(true);
        ClassPathSourceFactory classPathSourceFactory = new ClassPathSourceFactory();
        jfr.setSourceFactory(classPathSourceFactory);
        jfr.setOrder(0);
        jfr.setContentType("text/html;charset=UTF-8");
        //指定默认的日期格式化样式
        jfr.setDatePattern("yyyy-MM-dd HH:mm:ss");
        //指定模板文件存放位置
        jfr.setBaseTemplatePath("/templates/");
        //添加SharedMethod的类
        jfr.addSharedMethod(new EnjoySharedMethod());

        return jfr;
    }

}
