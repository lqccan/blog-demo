package com.example.demo.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/")
public class TestController {

    @GetMapping("/test")
    public String test(Model model) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            list.add("item" + (i + 1));
        }
        model.addAttribute("list", list);
        Date now = new Date();
        model.addAttribute("now", now);
        return "test.html";
    }

}
