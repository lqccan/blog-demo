package com.example.demo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * 环境信息配置
 */
@Configuration
public class ProfileConfig {

    @Value("${spring.profiles.active}")
    private String profiles;

    /**
     * 当前是否是开发环境
     * @return
     */
    public boolean isDev(){
        return "dev".equalsIgnoreCase(profiles);
    }

}
