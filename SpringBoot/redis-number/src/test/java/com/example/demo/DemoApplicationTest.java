package com.example.demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class DemoApplicationTest {

    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    public void testLong() {
        //往缓存写入数据
        Long l1 = 1L;
        Long l2 = Integer.MAX_VALUE + 1L;
        String k1 = "k1";
        String k2 = "k2";
        redisTemplate.opsForValue().set(k1, l1);
        redisTemplate.opsForValue().set(k2, l2);

        //从缓存中读取数据
        Object o1 = redisTemplate.opsForValue().get(k1);
        Object o2 = redisTemplate.opsForValue().get(k2);
        System.out.println("k1对应value类型"+o1.getClass().getName());
        System.out.println("k2对应value类型"+o2.getClass().getName());

        //强制使用Long类型会报异常
        try {
            Long o3 = (Long) redisTemplate.opsForValue().get(k1);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        //解决方法
        Number n1 = (Number) redisTemplate.opsForValue().get(k1);
        System.out.println("需要int时"+n1.intValue());
        System.out.println("需要long时"+n1.longValue());
    }

}