package com.example.demo.service;

import com.example.demo.entity.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    /**
     * 模拟数据获取，直接创建几条数据返回
     * @return
     */
    public List<User> listAll() {
        List<User> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            User user = new User();
            user.setId((long) i);
            user.setEmail("email" + i + "@qq.com");
            user.setAge(i);
            user.setName("小明" + i);
            list.add(user);
        }
        return list;
    }

}
