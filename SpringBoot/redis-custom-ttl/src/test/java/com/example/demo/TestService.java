package com.example.demo;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class TestService {

    private String value;

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    /**
     * 缓存获取value
     * @return
     */
    @Cacheable(cacheNames = "testCache#5", key = "'testKey'")
    public String getValueByCache() {
        System.out.println("加载数据到缓存");
        return this.getValue();
    }

}
