package com.example.demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class DemoApplicationTest {

    @Autowired
    private TestService testService;

    @Test
    public void testAnnotation() throws InterruptedException {
        testService.setValue("1");
        testService.getValue();
        testService.getValueByCache();
        testService.getValueByCache();
        testService.getValueByCache();
        System.out.println("等待缓存过期");
        Thread.sleep(5 * 1000);
        testService.getValueByCache();
    }

}