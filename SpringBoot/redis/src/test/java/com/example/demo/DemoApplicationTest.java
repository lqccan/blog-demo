package com.example.demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class DemoApplicationTest {

    @Autowired
    private TestService testService;

    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    public void testAnnotation() {
        testService.setValue("1");
        System.out.println("正常获取:"+testService.getValue());
        System.out.println("缓存获取:"+testService.getValueByCache());
        System.out.println("==============");

        testService.setValue("2");
        System.out.println("正常获取:"+testService.getValue());
        System.out.println("缓存获取:"+testService.getValueByCache());
        System.out.println("==============");

        testService.setValueAndEvictCache("3");
        System.out.println("正常获取:"+testService.getValue());
        System.out.println("缓存获取:"+testService.getValueByCache());
        System.out.println("==============");
    }

    @Test
    public void testRedisTemplate() {
        redisTemplate.opsForValue().set("key","value");
        System.out.println(redisTemplate.opsForValue().get("key"));
    }

}