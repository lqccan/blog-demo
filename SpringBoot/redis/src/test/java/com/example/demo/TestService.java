package com.example.demo;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class TestService {

    /**
     * 缓存名
     */
    public static final String CACHE_NAMES = "testCache";

    /**
     * 缓存key
     */
    public static final String KEY = "testKey";

    private String value;

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    /**
     * 缓存获取value
     * @return
     */
    @Cacheable(cacheNames = CACHE_NAMES, key = "#root.target.KEY")
    public String getValueByCache() {
        return this.getValue();
    }

    @CacheEvict(cacheNames = CACHE_NAMES, key = "#root.target.KEY")
    public void setValueAndEvictCache(String value) {
        setValue(value);
    }

}
