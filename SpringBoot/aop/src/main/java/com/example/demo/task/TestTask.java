package com.example.demo.task;

import cn.hutool.core.date.DateUtil;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class TestTask {

    /**
     * 测试定时任务
     * 1分钟执行1次
     */
    @Scheduled(cron = "0 * * * * ?")
    public void test() {
        System.out.println(DateUtil.now()+" 定时任务开始，模拟业务执行暂停3秒");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(DateUtil.now()+" 定时任务结束");
    }

}
