package com.example.demo.service;

import com.example.demo.startup.Startup;
import org.springframework.stereotype.Service;

@Service
public class BService {

    @Startup(1)
    public void startup(){
        System.out.println("BService startup!");
    }

}
