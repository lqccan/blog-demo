package com.example.demo.startup;

import java.lang.annotation.*;

/**
 * 系统启动注解
 * 被该注解标记的方法会在系统启动后即ApplicationReadyEvent事件时调用一次
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Startup {
    /**
     * 启动顺序（从小到大）
     *
     * @return
     */
    int value() default 100;
}
