package com.example.demo.service;

import com.example.demo.startup.Startup;
import org.springframework.stereotype.Service;

@Service
public class AService {

    @Startup
    public void startup(){
        System.out.println("AService startup!");
    }

}
