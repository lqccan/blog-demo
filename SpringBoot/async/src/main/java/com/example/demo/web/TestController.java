package com.example.demo.web;

import com.example.demo.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class TestController {

    @Autowired
    private TestService testService;

    @GetMapping("/test")
    public String test() {
        System.out.println("web请求使用线程");
        System.out.println(Thread.currentThread().getName());
        System.out.println("-------------");
        testService.test();
        testService.asyncTest();
        return "test";
    }

}
