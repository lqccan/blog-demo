package com.example.demo.sort.impl;

import com.example.demo.DemoApplicationTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class BubbleSortTest extends DemoApplicationTest {

    @Autowired
    private BubbleSort bubbleSort;

    @Test
    public void sortArray() {
        checkUtil.run(bubbleSort);
    }

}