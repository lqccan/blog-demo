package com.example.demo.sort.impl;

import com.example.demo.DemoApplicationTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class SelectionSortTest extends DemoApplicationTest {

    @Autowired
    private SelectionSort selectionSort;

    @Test
    public void sortArray() {
        checkUtil.run(selectionSort);
    }

}