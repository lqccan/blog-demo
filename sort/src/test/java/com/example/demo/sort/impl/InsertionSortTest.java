package com.example.demo.sort.impl;

import com.example.demo.DemoApplicationTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class InsertionSortTest extends DemoApplicationTest {

    @Autowired
    private InsertionSort insertionSort;

    @Test
    public void sortArray() {
        checkUtil.run(insertionSort);
    }
}