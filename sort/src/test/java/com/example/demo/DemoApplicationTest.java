package com.example.demo;

import com.example.demo.sort.Sort;
import com.example.demo.util.CheckUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class DemoApplicationTest {

    @Autowired
    protected List<Sort> sortList;

    @Autowired
    protected CheckUtil checkUtil;

    @Test
    public void testAll() {
        checkUtil.run(sortList);
    }

}