package com.example.demo.sort.impl;

import com.example.demo.sort.Sort;
import org.springframework.stereotype.Service;

/**
 * 选择排序
 */
@Service
public class SelectionSort implements Sort {

    @Override
    public String getName() {
        return "选择排序";
    }

    @Override
    public int[] sortArray(int[] nums) {
        int t, max;
        for (int i = 0; i < nums.length; i++) {
            //寻找max下标位置
            max = 0;
            for (int j = 0; j < nums.length - i; j++) {
                if (nums[j] > nums[max]) {
                    max = j;
                }
            }
            //交换
            t = nums[max];
            nums[max] = nums[nums.length - 1 - i];
            nums[nums.length - 1 - i] = t;
        }
        return nums;
    }

}
