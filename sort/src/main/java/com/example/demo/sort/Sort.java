package com.example.demo.sort;

/**
 * 排序接口
 */
public interface Sort {

    /**
     * 排序名
     * @return
     */
    String getName();

    /**
     * 排序函数
     * @param nums
     * @return
     */
    int[] sortArray(int[] nums);

}
