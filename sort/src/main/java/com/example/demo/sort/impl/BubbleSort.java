package com.example.demo.sort.impl;

import com.example.demo.sort.Sort;
import org.springframework.stereotype.Service;

/**
 * 冒泡排序
 */
@Service
public class BubbleSort implements Sort {

    @Override
    public String getName() {
        return "冒泡排序";
    }

    @Override
    public int[] sortArray(int[] nums) {
        int t;
        for (int i = 0; i < nums.length - 1; i++) {
            for (int j = 0; j < nums.length - 1 - i; j++) {
                if (nums[j] > nums[j + 1]) {
                    t = nums[j];
                    nums[j] = nums[j + 1];
                    nums[j + 1] = t;
                }
            }
        }
        return nums;
    }

}
