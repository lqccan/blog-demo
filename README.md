# 博客代码分享demo

## 介绍
存放博客分享的一些代码demo

## 目录
- demo 基础环境
- sort [排序算法合辑及测试代码](https://qclog.cn/1210)
- SpringBoot
    - aop [SpringBoot之使用aop切面记录@Scheduled定时任务开始时间和结束时间](https://qclog.cn/1054)
    - async [SpringBoot使用@Async注解实现异步处理](https://qclog.cn/1104)
    - enjoy [SpringBoot使用jfinal的enjoy作为视图模板引擎](https://qclog.cn/1069)
    - enjoy-devtools [SpringBoot中enjoy模板引擎与devtools冲突解决办法](https://qclog.cn/1148)
    - interceptor [SpringBoot之HandlerInterceptor拦截器的使用](https://qclog.cn/1152)
    - jxls [SpringBoot使用jxls组件实现可定制excel文件导出功能](https://qclog.cn/1086)
    - mail [SpringBoot发送文字邮件和html类型邮件](https://qclog.cn/1070)
    - redis [SpringBoot之redis缓存的基本配置及使用](https://qclog.cn/1063)
    - redis-custom-ttl [SpringBoot之@Cacheable注解改造实现自定义缓存过期时间配置](https://qclog.cn/1064)
    - redis-lock [SpringBoot使用redis搭配lua脚本实现分布式并发锁](https://qclog.cn/1109)
    - redis-number [SpringBoot之RedisTemplate存取Long类型数据自动变Integer问题](https://qclog.cn/1330)
    - redis-serializer [SpringBoot之RedisTemplate操作redis出现\xAC\xED\x00\x05t\x00\x08乱码问题](https://qclog.cn/1118)
    - startup [SpringBoot之一个注解实现开机启动（容器启动之后自动运行某些方法）](https://qclog.cn/1281)
    - war [SpringBoot之打成war包便于使用外置tomcat部署](https://qclog.cn/1039)
    - web-socket [SpringBoot使用WebSocket实现前后端消息推送](https://qclog.cn/1073)
    - demo14 [SpringBoot使用Redis+SpringEL表达式实现分布式并发锁注解](https://qclog.cn/1092)
- 前端
    - 图片loading [通过js代码切入全局实现图片loading效果](https://qclog.cn/1153)
    - 复制代码 [给代码块pre标签增加一个“复制代码”按钮](https://qclog.cn/1060)
    - 打赏 [给博客添加一个简单的打赏功能](https://qclog.cn/1043)
    - 正方形照片墙 [CSS实现宽度百分比，高度跟宽度一样的正方形效果](https://qclog.cn/1041)
    - 浏览器 [网上扒的一个CSS实现的浏览器样式](https://qclog.cn/1042)
- 后端
    - list-to-tree [Java实现列表转树形结构的两种方式list转tree](https://qclog.cn/1088)
    - MyBatis-Generator [使用MyBatis Generator生成数据表对应的实体、dao、xml文件](https://qclog.cn/1074)
    - ojdbc [使用maven的systemPath本地引用oracle的jdbc驱动包](https://qclog.cn/1112)
    - ojdbc-package [使用maven的systemPath本地引用jar包打包会丢失jar问题](https://qclog.cn/1114)
    - sharding-proxy-4.1.1 [sharding-proxy 4.1.1实践记录（附完整实践代码）](https://qclog.cn/1256)





