package com.example.demo.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class TestController {

    @GetMapping("/test")
    public String test() {
        try {
            Class<?> clazz = Class.forName("oracle.jdbc.driver.OracleDriver");
            return "引用成功"+clazz.getName();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return "jar包找不到";
    }

}
