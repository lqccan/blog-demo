drop table if exists t_user_0;
create table t_user_0(
    `id` bigint(20) not null,
    `name` varchar(100) not null default '' comment '用户名',
    primary key (`id`)
) engine=InnoDB default charset=utf8mb4 collate=utf8mb4_bin comment='用户表';
drop table if exists t_user_1;
drop table if exists t_user_2;
create table t_user_1 like t_user_0;
create table t_user_2 like t_user_0;


drop table if exists t_order_0;
create table t_order_0(
    `id` bigint(20) not null,
    `user_id` bigint(20) not null default 0 comment '用户id',
    primary key (`id`)
) engine=InnoDB default charset=utf8mb4 collate=utf8mb4_bin comment='订单表';
drop table if exists t_order_1;
drop table if exists t_order_2;
create table t_order_1 like t_order_0;
create table t_order_2 like t_order_0;


drop table if exists t_city;
create table t_city(
    `id` bigint(20) not null,
    `name` varchar(100) not null default '' comment '城市名',
    primary key (`id`)
) engine=InnoDB auto_increment=1 default charset=utf8mb4 collate=utf8mb4_bin comment='城市表';
insert into t_city(`id`,`name`) value(1,'北京');
insert into t_city(`id`,`name`) value(2,'上海');

