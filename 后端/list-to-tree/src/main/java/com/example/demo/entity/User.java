package com.example.demo.entity;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class User {

    /**
     * 用户id
     */
    private Long id;

    /**
     * 用户名
     */
    private String name;

    /**
     * 父id，即上级id
     */
    private Long parentId;

    /**
     * 子节点列表，即直系下级列表
     */
    private List<User> children;

    /**
     * 构造函数
     * @param id
     * @param name
     * @param parentId
     */
    public User(Long id, String name, Long parentId) {
        this.id = id;
        this.name = name;
        this.parentId = parentId;
    }
}
