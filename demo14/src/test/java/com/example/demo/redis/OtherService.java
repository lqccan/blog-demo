package com.example.demo.redis;

import org.springframework.stereotype.Service;

@Service
public class OtherService {

    /**
     * 不进行任何操作，只是模拟多个注解嵌套的场景
     */
    @RedisLock(key = "key")
    public void test() {
        System.out.println("OtherService.test");
    }

    @RedisLock(key = "'test'+#student.name+#teacher.name")
    public void test(Student student, Teacher teacher) {
        System.out.println("OtherService.test");
    }

}
