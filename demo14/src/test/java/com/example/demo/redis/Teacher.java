package com.example.demo.redis;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Teacher {

    private String name;

}
