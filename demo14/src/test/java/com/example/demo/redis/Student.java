package com.example.demo.redis;

import lombok.Data;
import lombok.ToString;

/**
 * 学生类
 */
@Data
@ToString
public class Student {

    private String name;

}
